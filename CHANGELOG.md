[2022-02-21]
- visualizar aba fotos e videos em lista ou icone
- paginação no Posts
- alterar senha

[2022-02-14]
- Passar Aba para o início do formulário

[2022-02-14]
- Alterado seta-polo-comunicacao-mo
- Alterar destaque das páginas pelo admin
- Alterado textos que ficam abaixo do ícone de cada página
- Removido lorems conforme pdf
- Adicionado botão de radio pra escolher o tipo
- Alterado de post para arquivo