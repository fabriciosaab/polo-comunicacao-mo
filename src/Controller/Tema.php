<?php

namespace TestProject\Controller;

class Tema extends Admin
{
    protected $oUtil, $oModel;
    private $_iId;

    public function __construct()
    {
        // Enable PHP Session
        if (empty($_SESSION))
            @session_start();

        $this->oUtil = new \TestProject\Engine\Util;

        /** Get the Model class in all the controller class **/
        $this->oUtil->getModel('Tema');
        $this->oModel = new \TestProject\Model\Tema;
    }
    
    public function temaList()
    {
        $this->isLogged();

        $this->oUtil->temas = $this->oModel->getAll();

        $this->oUtil->getView('admin/tema/temaList');
    }

    public function temaCreate()
    {
        $this->isLogged();

        $this->oUtil->getView('admin/tema/temaCreate');
    }

    public function temaStore()
    {
        $this->isLogged();

        if (isset($_POST['tema'])) {
            $data = array();
            $data['tema']    = $_POST['tema'];

            $result =  $this->oModel->store($data);
            // var_dump($result);die;

            if ($result) {
                header('Location: ' . ROOT_URL . '?p=tema&a=temaList');
                exit;
            }
            else
            {
                $this->oUtil->sErrMsg = 'Erro ao criar Tema';
                $this->oUtil->getView('admin/tema/temaCreate');
            }
        } else {
            $this->oUtil->sErrMsg = 'Preencha todos os dados obrigatórios';
            $this->oUtil->getView('admin/tema/temaCreate');
        }
    }

    public function temaEdit()
    {
        $this->isLogged();

        if (isset($_GET['id'])) {
            $this->oUtil->tema = $this->oModel->getById($_GET['id']);

            if ($this->oUtil->tema) {
                $this->oUtil->getView('admin/tema/temaEdit');
            } else {
                echo 'Tema não encontrado';
            }
        } else {
            echo 'ID não informado';
        }
    }

    public function temaUpdate()
    {
        $this->isLogged();

        if (isset(
            $_GET['id'],
            $_POST['tema']
        )) {
            $data = array();
            $data['id']   = $_GET['id'];
            $data['tema'] = $_POST['tema'];
            
            $result =  $this->oModel->update($data);
            // var_dump($result);die;

            if ($result) {
                header('Location: ' . ROOT_URL . '?p=tema&a=temaList');
                exit;
            }
            else
            {
                echo 'Erro ao editar Tema';
            }
        } else {
            echo 'Preencha todos os dados obrigatórios';
        }
    }

    public function temaDestroy()
    {
        $this->isLogged();

        if (isset($_GET['id'])) {
            $result = $this->oModel->destroy($_GET['id']);

            if ($result) {
                header('Location: ' . ROOT_URL . '?p=tema&a=temaList');
                exit;
            } else {
                echo 'Erro ao excluir Tema';
            }
        } else {
            echo 'ID não informado';
        }
    }
}