<?php

namespace TestProject\Controller;

class Area extends Admin
{
    protected $oUtil, $oModel;

    public function __construct()
    {
        // Enable PHP Session
        if (empty($_SESSION))
            @session_start();

        $this->oUtil = new \TestProject\Engine\Util;

        /** Get the Model class in all the controller class **/
        $this->oUtil->getModel('Area');
        $this->oModel = new \TestProject\Model\Area;
    }
    
    public function areaList()
    {
        $this->isLogged();

        $this->oUtil->areas = $this->oModel->getAll();

        $this->oUtil->getView('admin/area/areaList');
    }

    public function areaCreate()
    {
        $this->isLogged();

        $this->oUtil->getView('admin/area/areaCreate');
    }

    public function areaStore()
    {
        $this->isLogged();

        if (isset($_POST['area'])) {
            $data = array();
            $data['area'] = $_POST['area'];

            $result =  $this->oModel->store($data);
            // var_dump($result);die;

            if ($result) {
                header('Location: ' . ROOT_URL . '?p=area&a=areaList');
                exit;
            }
            else
            {
                $this->oUtil->sErrMsg = 'Erro ao criar area';
                $this->oUtil->getView('admin/area/areaCreate');
            }
        } else {
            $this->oUtil->sErrMsg = 'Preencha todos os dados obrigatórios';
            $this->oUtil->getView('admin/area/areaCreate');
        }
    }

    public function areaEdit()
    {
        $this->isLogged();

        if (isset($_GET['id'])) {
            $this->oUtil->area = $this->oModel->getById($_GET['id']);

            if ($this->oUtil->area) {
                $this->oUtil->getView('admin/area/areaEdit');
            } else {
                echo 'Area não encontrado';
            }
        } else {
            echo 'ID não informado';
        }
    }

    public function areaUpdate()
    {
        $this->isLogged();

        if (isset(
            $_GET['id'],
            $_POST['area']
        )) {
            $data = array();
            $data['id']   = $_GET['id'];
            $data['area'] = $_POST['area'];
            
            $result =  $this->oModel->update($data);
            // var_dump($result);die;

            if ($result) {
                header('Location: ' . ROOT_URL . '?p=area&a=areaList');
                exit;
            }
            else
            {
                echo 'Erro ao editar Area';
            }
        } else {
            echo 'Preencha todos os dados obrigatórios';
        }
    }

    public function areaDestroy()
    {
        $this->isLogged();

        if (isset($_GET['id'])) {
            $result = $this->oModel->destroy($_GET['id']);

            if ($result) {
                header('Location: ' . ROOT_URL . '?p=area&a=areaList');
                exit;
            } else {
                echo 'Erro ao excluir Area';
            }
        } else {
            echo 'ID não informado';
        }
    }
}