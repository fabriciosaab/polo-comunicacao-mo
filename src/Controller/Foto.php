<?php

namespace TestProject\Controller;

class Foto extends Admin
{
    protected $oUtil, $oModel;
    private $_iId;

    public function __construct()
    {
        // Enable PHP Session
        if (empty($_SESSION))
            @session_start();

        $this->oUtil = new \TestProject\Engine\Util;

        /** Get the Model class in all the controller class **/
        $this->oUtil->getModel('FotoVideo');
        $this->oModel = new \TestProject\Model\FotoVideo;
    }

    public function index()
    {
        if (isset($_GET['id'])) {
            $id = $_GET['id'];
            $this->oUtil->arquivo = $this->oModel->getById($id);
        } else {
            $id = 0;
            $this->oUtil->arquivo = false;
        }

        $this->oUtil->arquivos = $this->oModel->list($id);

        // Destaque
        $this->oUtil->getModel('Pagina');
        $PaginaModel = new \TestProject\Model\Pagina;
        $destaque = $PaginaModel->getall();
        $this->oUtil->destaque = $destaque[0]->destaque_foto;
        

        $this->oUtil->getView('fotoVideo');
    }
    
    public function list()
    {
        $this->isLogged();

        if (isset($_GET['id'])) {
            $id = $_GET['id'];
            $this->oUtil->arquivo = $this->oModel->getById($id);
        } else {
            $id = 0;
            $this->oUtil->arquivo = false;
        }

        $this->oUtil->arquivos = $this->oModel->list($id);
        

        $this->oUtil->getView('admin/foto/list');
    }

    public function criarPasta()
    {
        $this->isLogged();

        if (isset($_POST['pasta'])) {
            $data = array();
            $data['nome'] = $_POST['pasta'];
            $data['parent_id'] = $_POST['parent_id'];            
            $data['path'] = "null";
            $data['link'] = "null";
            $data['tipo'] = "pasta";

            $result =  $this->oModel->store($data);
            // var_dump($result);die;

            if ($result) {
                header('Location: ' . ROOT_URL . '?p=foto&a=list');
                exit;
            }
            else
            {
                $this->oUtil->sErrMsg = 'Erro ao criar Foto';
                $this->oUtil->getView('admin/foto/create');
            }
        } else {
            $this->oUtil->sErrMsg = 'Preencha todos os dados obrigatórios';
            $this->oUtil->getView('admin/foto/create');
        }
    }

    public function uploadArquivo()
    {
        $this->isLogged();

        $arquivo = $this->uploadFile(); // File upload

        $data = array();
        $data['nome'] = $arquivo['nome'];
        $data['parent_id'] = $_POST['parent_id'];            
        $data['path'] = $arquivo['path'];
        $data['link'] = $arquivo['link'];
        $data['tipo'] = "arquivo";

        $result =  $this->oModel->store($data);
        // var_dump($result);die;

        if ($result) {
            header('Location: ' . ROOT_URL . '?p=foto&a=list&id=' . $_POST['parent_id']);
            exit;
        }
        else
        {
            $this->oUtil->sErrMsg = 'Erro ao criar Foto';
            $this->oUtil->getView('admin/foto/create');
        }
    }

    public function edit()
    {
        $this->isLogged();

        if (isset($_GET['id'])) {
            $this->oUtil->foto = $this->oModel->getById($_GET['id']);

            if ($this->oUtil->foto) {
                $this->oUtil->getView('admin/foto/edit');
            } else {
                echo 'Foto não encontrado';
            }
        } else {
            echo 'ID não informado';
        }
    }

    public function update()
    {
        $this->isLogged();

        if (isset(
            $_GET['id'],
            $_POST['nome']
        )) {
            $data = array();
            $data['id']   = $_GET['id'];
            $data['nome'] = $_POST['nome'];
            
            $result =  $this->oModel->update($data);
            // var_dump($result);die;

            if ($result) {
                header('Location: ' . ROOT_URL . '?p=foto&a=list');
                exit;
            }
            else
            {
                echo 'Erro ao editar Pasta';
            }
        } else {
            echo 'Preencha todos os dados obrigatórios';
        }
    }

    public function destroy()
    {
        $this->isLogged();

        if (isset($_GET['id'])) {
            $result = $this->destroyFolderFile($_GET['id']);
            
            if ($result) {
                header('Location: ' . ROOT_URL . '?p=foto&a=list');
                exit;
            } else {
                echo 'Erro ao excluir Foto';
            }
        } else {
            echo 'ID não informado';
        }
    }

    private function destroyFolderFile($id)
    {
        $arquivo = $this->oModel->getById($id);

        if ($arquivo->tipo == 'pasta') {
            $childrensFiles = $this->oModel->getChildrens($id);
            
            foreach ($childrensFiles as $children) {
                $this->destroyFolderFile($children->id);
            }
        } else {
            $this->rrmdir($arquivo->path);
        }

        $result = $this->oModel->destroy($id);

        if ($result) {
            return true;
        } else {
            return false;
        }
    }
}