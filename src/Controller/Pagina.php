<?php

namespace TestProject\Controller;

class Pagina extends Admin
{
    const MAX_POSTS = 10;

    protected $oUtil, $oModel;
    private $_iId;

    public function __construct()
    {
        // Enable PHP Session
        if (empty($_SESSION))
            @session_start();

        $this->oUtil = new \TestProject\Engine\Util;
        $this->oUtil->maxPost = self::MAX_POSTS;
    }

    public function index()
    {
        $this->oUtil->getModel('Pagina');
        $Pagina = new \TestProject\Model\Pagina;

        $this->oUtil->paginas = $Pagina->getAll();

        // var_dump($this->oUtil->Paginas);die;

        $this->oUtil->getView('admin/pagina/index');
    }

    public function paginaUpdate()
    {
        $this->isLogged();

        if (isset(
            $_POST['destaque_informa'],
            $_POST['destaque_mural'],
            $_POST['destaque_foto']
        )) {
            $this->oUtil->getModel('Pagina');
            $PaginaModel = new \TestProject\Model\Pagina;

            $data = array();
            $data['destaque_informa'] = $_POST['destaque_informa'];
            $data['destaque_mural']   = $_POST['destaque_mural'];
            $data['destaque_foto']    = $_POST['destaque_foto'];
            
            $result =  $PaginaModel->update($data);
            // var_dump($result);die;

            if ($result) {
                header('Location: ' . ROOT_URL . '?p=pagina');
                exit;
            }
            else
            {
                echo 'Erro ao editar Pagina';
            }
        } else {
            echo 'Preencha todos os dados obrigatórios';
        }
    }

}