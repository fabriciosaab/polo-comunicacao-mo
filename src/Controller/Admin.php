<?php

namespace TestProject\Controller;

class Admin
{
    public function __construct()
    {
        // Enable PHP Session
        if (empty($_SESSION))
            @session_start();

        $this->oUtil = new \TestProject\Engine\Util;
    }

    public function index()
    {
        $this->isLogged();

        $this->oUtil->getView('admin/index');
    }

    public function login()
    {
        // $this->isLogged();

        if (isset($_POST['email'], $_POST['password']))
        {
            $this->oUtil->getModel('Admin');
            $this->oModel = new \TestProject\Model\Admin;

            $sHashPassword =  $this->oModel->login($_POST['email']);
            if (password_verify($_POST['password'], $sHashPassword))
            {
                $_SESSION['is_logged'] = 1; // Admin is logged now
                header('Location: ' . ROOT_URL . '?p=admin');
                exit;
            }
            else
            {
                $this->oUtil->sErrMsg = 'Incorrect Login!';
            }
        }

        $this->oUtil->getView('admin/login');
    }

    public function logout()
    {
        $this->isLogged();

        // If there is a session, destroy it to disconnect the admin
        if (!empty($_SESSION))
        {
            $_SESSION = array();
            session_unset();
            session_destroy();
        }

        // Redirect to the homepage
        header('Location: ' . ROOT_URL);
        exit;
    }

    public function changePassword() {
        $this->isLogged();

        $this->oUtil->getView('admin/changePassword');
    }

    public function updatePassword() {
        $this->isLogged();

        if (isset(
            $_POST['current_password'],
            $_POST['new_password'],
            $_POST['password_confirmation']
        )) {
            if ($_POST['new_password'] != $_POST['password_confirmation']) {
                $this->oUtil->sErrMsg = 'Senhas não conferem';
                $this->oUtil->getView('admin/changePassword');
                exit;
            }

            $this->oUtil->getModel('Admin');
            $AdminModel = new \TestProject\Model\Admin;

            $result =  $AdminModel->updatePassword($_POST['new_password']);

            if ($result) {
                header('Location: ' . ROOT_URL . '?p=admin');
                exit;
            } else {
                $this->oUtil->sErrMsg = 'Erro ao alterar a senha';
                $this->oUtil->getView('admin/changePassword');
            }
        } else {
            $this->oUtil->sErrMsg = 'Preencha todos os dados obrigatórios';
            $this->oUtil->getView('admin/changePassword');
        }
    }

    public function notFound()
    {
        echo "Página não encontrada";
    }

    protected function isLogged()
    {
        if (empty($_SESSION['is_logged'])) {
            header('Location: ' . ROOT_URL . '?p=admin&a=login');
        }
    }

    public function uploadFile() {
        var_dump($_FILES);
        // Pasta onde o arquivo vai ser salvo
        $_UP['pasta'] = \TestProject\Engine\Config::STORAGE_PATH;
        
        // Tamanho máximo do arquivo (em Bytes)
        $_UP['tamanho'] = 1024 * 1024 * 100; // 2Mb
        
        // Array com as extensões permitidas
        $_UP['extensoes'] = array('jpg', 'png', 'gif', 'jpeg', 'pdf', 'mp4', 'mp3', 'avi', 'doc', 'docx', 'xls', 'xlsx');
        
        // Array com os tipos de erros de upload do PHP
        $_UP['erros'][0] = 'Não houve erro';
        $_UP['erros'][1] = 'O arquivo no upload é maior do que o limite do PHP';
        $_UP['erros'][2] = 'O arquivo ultrapassa o limite de tamanho especifiado no HTML';
        $_UP['erros'][3] = 'O upload do arquivo foi feito parcialmente';
        $_UP['erros'][4] = 'Não foi feito o upload do arquivo';
        
        // Verifica se houve algum erro com o upload. Se sim, exibe a mensagem do erro
        if ($_FILES['arquivo']['error'] != 0) {
            die("Não foi possível fazer o upload, erro:<br />" . $_UP['erros'][$_FILES['arquivo']['error']]);
        }
        
        // Caso script chegue a esse ponto, não houve erro com o upload e o PHP pode continuar
        
        // Faz a verificação da extensão do arquivo
        $extensao = strtolower(end(explode('.', $_FILES['arquivo']['name'])));
        if (array_search($extensao, $_UP['extensoes']) === false) {
            echo "Extensão de arquivo não permitida";
            die;
        }
        
        // Faz a verificação do tamanho do arquivo
        else if ($_UP['tamanho'] < $_FILES['arquivo']['size']) {
            echo "O arquivo enviado é muito grande, envie arquivos de até 2Mb.";
            die;
        }
        
        // O arquivo passou em todas as verificações, hora de tentar movê-lo para a pasta
        else {
            $arquivo = time().'-'.$_FILES['arquivo']['name'];
            $arquivo_nome = $_FILES['arquivo']['name'];
            
            // Depois verifica se é possível mover o arquivo para a pasta escolhida
            if (move_uploaded_file($_FILES['arquivo']['tmp_name'], $_UP['pasta'] . $arquivo)) {
                // Upload efetuado com sucesso, exibe uma mensagem e um link para o arquivo
                
                $return = array();
                $return['nome'] = $arquivo_nome;
                $return['path'] = $_UP['pasta'] . $arquivo;
                $return['link'] = \TestProject\Engine\Config::STORAGE_LINK . $arquivo;

                return $return;
            } else {
                // Não foi possível fazer o upload, provavelmente a pasta está incorreta
                echo "Não foi possível enviar o arquivo, tente novamente";
                die;
            }
        }
    }

    public function rrmdir($dir) {
        if (is_dir($dir)) { 
            $objects = scandir($dir);
            foreach ($objects as $object) { 
                if ($object != "." && $object != "..") { 
                    if (is_dir($dir. DIRECTORY_SEPARATOR .$object) && !is_link($dir."/".$object)) {
                        $this->rrmdir($dir. DIRECTORY_SEPARATOR .$object);
                    } else {
                        unlink($dir. DIRECTORY_SEPARATOR .$object); 
                    }
                } 
            }
            $this->rmdir($dir);  
        } else {
            unlink($dir);
        } 
    }
}