<?php

namespace TestProject\Controller;

class Home
{
    const MAX_POSTS = 10;

    protected $oUtil, $oModel;
    private $_iId;

    public function __construct()
    {
        // Enable PHP Session
        if (empty($_SESSION))
            @session_start();

        $this->oUtil = new \TestProject\Engine\Util;
        $this->oUtil->maxPost = self::MAX_POSTS;
    }


    /***** Front end *****/
    // Homepage
    public function index()
    {
        if (isset($_GET['offset'])) {
            if ($_GET['offset'] < 0 ) {
                $this->oUtil->offset = 0;
            } else {
                $this->oUtil->offset = $_GET['offset'];
            }
        } else {
            $this->oUtil->offset = 0;
        }

        $params = array();
        $params['tipo'] = 'informa';

        if (isset($_POST['busca'])) { $params['busca'] = $_POST['busca']; }
        else if (isset($_GET['tema'])) { $params['tema'] = $_GET['tema']; }
        if (isset($_GET['area'])) { $params['area'] = $_GET['area']; }
        
        // Posts
        $this->oUtil->getModel('Post');
        $PostModel = new \TestProject\Model\Post;
        $this->oUtil->oPosts = $PostModel->get($this->oUtil->offset, self::MAX_POSTS, $params);
        $this->oUtil->count = $PostModel->count($params);

        // Temas
        $this->oUtil->getModel('Tema');
        $TemaModel = new \TestProject\Model\Tema;
        $this->oUtil->temas = $TemaModel->getall();

        // Areas
        $this->oUtil->getModel('Area');
        $AreaModel = new \TestProject\Model\Area;
        $this->oUtil->areas = $AreaModel->getall();

        // url
        $this->oUtil->url = $this->oUtil->addToUrl($_SERVER['REQUEST_URI']);

        // Destaque
        $this->oUtil->getModel('Pagina');
        $PaginaModel = new \TestProject\Model\Pagina;
        $destaque = $PaginaModel->getall();
        $this->oUtil->destaque = $destaque[0]->destaque_informa;

        $this->oUtil->getView('index');
    }
}
