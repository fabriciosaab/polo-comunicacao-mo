<?php

namespace TestProject\Controller;

class Post extends Admin
{
    const MAX_POSTS = 20;

    protected $oUtil, $oModel;
    private $_iId;

    public function __construct()
    {
        // Enable PHP Session
        if (empty($_SESSION))
            @session_start();

        $this->oUtil = new \TestProject\Engine\Util;

        $this->oUtil->maxPost = self::MAX_POSTS;

        /** Get the Model class in all the controller class **/
        // $this->oUtil->getModel('Post');
        // $this->oModel = new \TestProject\Model\Post;
    }
    
    public function postList()
    {
        $this->isLogged();

        if (isset($_GET['offset'])) {
            if ($_GET['offset'] < 0 ) {
                $this->oUtil->offset = 0;
            } else {
                $this->oUtil->offset = $_GET['offset'];
            }
        } else {
            $this->oUtil->offset = 0;
        }
        
        $this->oUtil->getModel('Post');
        $Post = new \TestProject\Model\Post;

        $this->oUtil->posts = $Post->getAll($this->oUtil->offset, $this->oUtil->maxPost);

        $params = array();
        $this->oUtil->count = $Post->count($params);

        $this->oUtil->getView('admin/post/postList');
    }

    public function postCreate()
    {
        $this->isLogged();

        // Temas
        $this->oUtil->getModel('Tema');
        $TemasModel = new \TestProject\Model\Tema;
        $this->oUtil->temas = $TemasModel->getAll();

        // Areas
        $this->oUtil->getModel('Area');
        $AreasModel = new \TestProject\Model\Area;
        $this->oUtil->areas = $AreasModel->getAll();

        $this->oUtil->getView('admin/post/postCreate');
    }

    public function postStore()
    {
        $this->isLogged();

        if (isset(
                $_POST['tema'],
                $_POST['assunto'],
                $_POST['area'],
                $_FILES['arquivo'],
                $_POST['tipo']
            )) {
                $this->oUtil->getModel('Post');
                $PostModel = new \TestProject\Model\Post;

                $data = array();
                $data['tema']    = $_POST['tema'];
                $data['assunto'] = $_POST['assunto'];
                $data['area']    = $_POST['area'];
                
                $arquivo = $this->uploadFile(); // File upload
                $data['path']         = $arquivo['path'];
                $data['arquivo_nome'] = $arquivo['nome'];
                $data['link']         = $arquivo['link'];

                $data['tipo'] = $_POST['tipo'];

            $result =  $PostModel->store($data);

            if ($result) {
                header('Location: ' . ROOT_URL . '?p=post&a=postList');
                exit;
            }
            else
            {
                $this->oUtil->sErrMsg = 'Erro ao criar Post';
                $this->oUtil->getView('admin/post/postCreate');
            }
        } else {
            $this->oUtil->sErrMsg = 'Preencha todos os dados obrigatórios';
            $this->oUtil->getView('admin/post/postCreate');
        }
    }

    public function postEdit()
    {
        $this->isLogged();

        if (isset($_GET['id'])) {
            $this->oUtil->getModel('Post');
            $PostModel = new \TestProject\Model\Post;

            $this->oUtil->post = $PostModel->getById($_GET['id']);

            if ($this->oUtil->post) {
                // Temas
                $this->oUtil->getModel('Tema');
                $TemasModel = new \TestProject\Model\Tema;
                $this->oUtil->temas = $TemasModel->getAll();

                // Areas
                $this->oUtil->getModel('Area');
                $AreasModel = new \TestProject\Model\Area;
                $this->oUtil->areas = $AreasModel->getAll();
                
                $this->oUtil->getView('admin/post/postEdit');
            } else {
                echo 'Post não encontrado';
                // $this->oUtil->getView('admin/post/postList');
            }
        } else {
            echo 'ID não informado';
            // $this->oUtil->getView('admin/post/postList');
        }
    }

    public function postUpdate()
    {
        $this->isLogged();

        if (isset(
            $_GET['id'],
            $_POST['tema'],
            $_POST['assunto'],
            $_POST['area'],
            $_POST['tipo']
        )) {
            $this->oUtil->getModel('Post');
            $PostModel = new \TestProject\Model\Post;

            $data = array();
            $data['id']      = $_GET['id'];
            $data['tema']    = $_POST['tema'];
            $data['assunto'] = $_POST['assunto'];
            $data['area']    = $_POST['area'];
            $data['tipo']    = $_POST['tipo'];

            $result =  $PostModel->update($data);
            // var_dump($result);die;

            if ($result) {
                header('Location: ' . ROOT_URL . '?p=post&a=postList');
                exit;
            }
            else
            {
                echo 'Erro ao editar Post';
            }
        } else {
            echo 'Preencha todos os dados obrigatórios';
        }
    }

    public function postDestroy()
    {
        $this->isLogged();

        if (isset($_GET['id'])) {
            $this->oUtil->getModel('Post');
            $PostModel = new \TestProject\Model\Post;

            $result = $PostModel->destroy($_GET['id']);

            if ($result) {
                header('Location: ' . ROOT_URL . '?p=post&a=postList');
                exit;
            } else {
                echo 'Erro ao excluir Post';
            }
        } else {
            echo 'ID não informado';
        }
    }
}