<?php

namespace TestProject\Model;

class Tema
{
    protected $oDb;

    public function __construct()
    {
        $this->oDb = new \TestProject\Engine\Db;
    }

    public function getAll()
    {
        $oStmt = $this->oDb->query('SELECT * FROM Temas ORDER BY created_at DESC');
        return $oStmt->fetchAll(\PDO::FETCH_OBJ);
    }

    public function store(array $aData)
    {
        $aData['created_at'] = date("Y-m-d H:i:s");
        $aData['updated_at'] = date("Y-m-d H:i:s");

        $oStmt = $this->oDb->prepare('INSERT INTO Temas (tema, created_at, updated_at) VALUES(:tema, :created_at, :updated_at)');
        
        return $oStmt->execute($aData);
    }

    public function getById($iId)
    {
        $sql = "SELECT * FROM Temas WHERE id = $iId";
        $oStmt = $this->oDb->prepare($sql);
        $oStmt->execute();

        return $oStmt->fetch(\PDO::FETCH_OBJ);
    }

    public function update(array $aData)
    {
        $aData['updated'] = date("Y-m-d H:i:s");

        $sql = "UPDATE Temas SET tema = '".$aData['tema']."', updated_at = '".$aData['updated']."' WHERE id = ".$aData['id'];
        $oStmt = $this->oDb->prepare($sql);
        
        return $oStmt->execute();
    }

    public function destroy($iId)
    {
        $oStmt = $this->oDb->prepare('DELETE FROM Temas WHERE id = :id LIMIT 1');
        $oStmt->bindParam(':id', $iId, \PDO::PARAM_INT);
        return $oStmt->execute();
    }
}
