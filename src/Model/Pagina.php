<?php

namespace TestProject\Model;

class Pagina
{
    protected $oDb;

    public function __construct()
    {
        $this->oDb = new \TestProject\Engine\Db;
    }

    public function getAll()
    {
        $oStmt = $this->oDb->query('SELECT * FROM Paginas');
        return $oStmt->fetchAll(\PDO::FETCH_OBJ);
    }

    public function update(array $aData)
    {

        $sql = "Update Paginas SET destaque_informa = '".$aData['destaque_informa']."', destaque_mural = '".$aData['destaque_mural']."', destaque_foto = '".$aData['destaque_foto']."'";

        // var_dump($sql);die;
        $oStmt = $this->oDb->prepare($sql);
        
        return $oStmt->execute();
    }
}
