<?php

namespace TestProject\Model;

class Post
{
    protected $oDb;

    public function __construct()
    {
        $this->oDb = new \TestProject\Engine\Db;
    }

    public function get($iOffset, $iLimit, $params)
    {
        $where = "WHERE tipo = '".$params['tipo']."' ";
        if (isset($params['busca'])) { $where .= "AND assunto = '".$params['busca']."' "; }
        if (isset($params['tema'])) { $where .= "AND tema = '".$params['tema']."' "; }
        if (isset($params['area'])) { $where .= "AND area = '".$params['area']."' "; }

        $sqlSelect = "SELECT * FROM Posts " . $where . "ORDER BY created_at ASC LIMIT :offset, :limit";

        // var_dump($sqlSelect);
        $oStmt = $this->oDb->prepare($sqlSelect);
        $oStmt->bindParam(':offset', $iOffset, \PDO::PARAM_INT);
        $oStmt->bindParam(':limit', $iLimit, \PDO::PARAM_INT);
        $oStmt->execute();

        return $oStmt->fetchAll(\PDO::FETCH_OBJ);
    }

    public function count($params)
    {
        $where = "";
        
        if (isset($params['tipo'])) {
            $where = "WHERE tipo = '".$params['tipo']."' ";
        }

        if (isset($params['busca'])) { $where .= "AND assunto = '".$params['busca']."' "; }
        if (isset($params['tema'])) { $where .= "AND tema = '".$params['tema']."' "; }
        if (isset($params['area'])) { $where .= "AND area = '".$params['area']."' "; }

        $sql = "SELECT COUNT(id) as count FROM Posts " . $where;
        
        $oStmt = $this->oDb->query($sql);

        $result = $oStmt->fetch(\PDO::FETCH_OBJ);
        return $result->count;
    }

    public function getAll($offset , $limit)
    {
        $sql = "SELECT * FROM Posts ORDER BY created_at DESC LIMIT $offset, $limit";
        $oStmt = $this->oDb->query($sql);
        
        return $oStmt->fetchAll(\PDO::FETCH_OBJ);
    }

    public function store(array $aData)
    {
        $aData['created_at'] = date("Y-m-d H:i:s");
        $aData['updated_at'] = date("Y-m-d H:i:s");

        $oStmt = $this->oDb->prepare('INSERT INTO Posts (tema, assunto, area, path, arquivo_nome, link, tipo, created_at, updated_at) VALUES(:tema, :assunto, :area, :path, :arquivo_nome, :link, :tipo, :created_at, :updated_at)');
        
        return $oStmt->execute($aData);
    }

    public function getById($iId)
    {
        $sql = "SELECT * FROM Posts WHERE id = $iId";
        $oStmt = $this->oDb->prepare($sql);
        $oStmt->execute();

        return $oStmt->fetch(\PDO::FETCH_OBJ);
    }

    public function update(array $aData)
    {
        $aData['updated'] = date("Y-m-d H:i:s");

        $sql = "UPDATE Posts SET tema = '".$aData['tema']."', assunto = '".$aData['assunto']."', area = '".$aData['area']."', tipo = '".$aData['tipo']."', updated_at = '".$aData['updated']."' WHERE id = ".$aData['id'];
        $oStmt = $this->oDb->prepare($sql);
        
        return $oStmt->execute();
    }

    public function destroy($iId)
    {
        $oStmt = $this->oDb->prepare('DELETE FROM Posts WHERE id = :postId LIMIT 1');
        $oStmt->bindParam(':postId', $iId, \PDO::PARAM_INT);
        return $oStmt->execute();
    }
}
