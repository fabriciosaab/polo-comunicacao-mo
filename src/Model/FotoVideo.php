<?php

namespace TestProject\Model;

class FotoVideo
{
    protected $oDb;

    public function __construct()
    {
        $this->oDb = new \TestProject\Engine\Db;
    }

    public function list($parent_id = 0)
    {
        $query = "SELECT * FROM FotoVideo WHERE parent_id = $parent_id ORDER BY created_at DESC";
        $oStmt = $this->oDb->query($query);
        return $oStmt->fetchAll(\PDO::FETCH_OBJ);
    }

    public function store(array $aData)
    {
        $aData['created_at'] = date("Y-m-d H:i:s");
        $aData['updated_at'] = date("Y-m-d H:i:s");

        $oStmt = $this->oDb->prepare('INSERT INTO FotoVideo (nome, parent_id, path, link, tipo, created_at, updated_at) VALUES(:nome, :parent_id, :path, :link, :tipo, :created_at, :updated_at)');
        
        return $oStmt->execute($aData);
    }

    public function getById($iId)
    {
        $sql = "SELECT * FROM FotoVideo WHERE id = $iId";
        $oStmt = $this->oDb->prepare($sql);
        $oStmt->execute();

        return $oStmt->fetch(\PDO::FETCH_OBJ);
    }

    public function getChildrens($parent_id)
    {
        $sql = "SELECT * FROM FotoVideo WHERE parent_id = $parent_id";
        $oStmt = $this->oDb->prepare($sql);
        $oStmt->execute();

        return $oStmt->fetchAll(\PDO::FETCH_OBJ);
    }

    public function update(array $aData)
    {
        $aData['updated_at'] = date("Y-m-d H:i:s");

        $sql = "UPDATE FotoVideo SET nome = '".$aData['nome']."', updated_at = '".$aData['updated_at']."' WHERE id = ".$aData['id'];
        $oStmt = $this->oDb->prepare($sql);
        
        return $oStmt->execute();
    }

    public function destroy($iId)
    {
        $oStmt = $this->oDb->prepare('DELETE FROM FotoVideo WHERE id = :id LIMIT 1');
        $oStmt->bindParam(':id', $iId, \PDO::PARAM_INT);
        return $oStmt->execute();
    }
}
