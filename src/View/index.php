<?php require 'inc/header.php' ?>

<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="pt-5 text-center">
                <img class="p-5" src="<?= ROOT_URL ?>assets/img/icone-mo-informa.svg" alt="ÍNDICE DO MO INFORMA">
                <h2>FAÇA A BUSCA NO <span class="texto-azul">MO INFORMA</span></h2>
            </div>
        </div>
    </div>

    <div class="mt-5 mb-5">
        <form class="row g-2" method="post">
            <div class="col-sm-10">
                <input type="text" class="form-control input-busca" id="busca" name="busca" placeholder="Buscar">
            </div>
            <div class="col-sm-2">
                <button type="submit" class="btn btn-primary mb-3">BUSCAR</button>
            </div>
        </form>
    </div>

    <div class="row pt-3">
        <div class="row-sm-12">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">
                            <button class="btn" type="button">
                                DATA
                            </button>
                        </th>
                        
                        <th scope="col">
                            <div class="dropdown">
                                <button class="btn dropdown-toggle" type="button" id="dropdownArea" data-bs-toggle="dropdown" aria-expanded="false">
                                    TEMA
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dropdownArea">
                                    <?php foreach ($this->temas as $tema): ?>
                                        <li><a class="dropdown-item" href="<?= ROOT_URL ?>?tema=<?= $tema->tema ?>"><?= $tema->tema ?></a></li>
                                    <?php endforeach ?>
                                </ul>
                            </div>
                        </th>

                        <th scope="col">
                            <button class="btn">
                                ASSUNTO
                            </button>
                        </th>

                        <th scope="col">
                            <div class="dropdown">
                                <button class="btn dropdown-toggle" type="button" id="dropdownArea" data-bs-toggle="dropdown" aria-expanded="false">
                                    DEPARTAMENTO/ÁREA
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dropdownArea">
                                    <?php foreach ($this->areas as $area): ?>
                                        <li><a class="dropdown-item" href="<?= ROOT_URL ?>?area=<?= $area->area ?>"><?= $area->area ?></a></li>
                                    <?php endforeach ?>
                                </ul>
                            </div>	
                        </th>

                        <th scope="col">
                            <button class="btn" type="button">
                                ARQUIVOS
                            </button>	
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (empty($this->oPosts)): ?>
                        <tr>
                            <td>Sem arquivos</td>
                        </tr>
                    <?php else: ?>
                        <?php foreach ($this->oPosts as $post): ?>
                            <tr>
                                <td><?= $post->created_at ?></td>
                                <td><?= $post->tema ?></td>
                                <td><?= $post->assunto ?></td>
                                <td><?= $post->area ?></td>
                                <td style="text-align: center">
                                    <a href="<?= $post->link ?>" target="_blank"><img class="btn-baixar" src="<?= ROOT_URL ?>assets/img/btn-baixar.svg" alt="baixar"></a>
                                </td>
                            </tr>
                        <?php endforeach ?>
                    <?php endif ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row pt-5">
        <div class="col-sm-12">
            <nav aria-label="Page navigation example">
                <ul class="pagination justify-content-center">
                    <?php if ($this->offset == 0 ): ?>
                        <li class="page-item disabled"><a class="page-link" href="#">< ANTERIOR</a></li>
                    <?php else: ?>
                        <li class="page-item"><a class="page-link" href="<?= $this->url ?>offset=<?=$this->offset - $this->maxPost ?>">< ANTERIOR</a></li>
                    <?php endif ?>

                    <li class="page-item page-link page-position"><?= $this->offset / $this->maxPost + 1 ?> DE <?= ceil($this->count/$this->maxPost) ?></li>

                    <?php if ($this->offset / $this->maxPost + 1 >= ceil($this->count/$this->maxPost) ): ?>
                        <li class="page-item disabled"><a class="page-link" href="#">PRÓXIMA ></a></li>
                    <?php else: ?>
                        <li class="page-item"><a class="page-link" href="<?= $this->url ?>offset=<?= $this->offset + $this->maxPost ?>">PRÓXIMA ></a></li>
                    <?php endif ?>
                </ul>
            </nav>
        </div>
    </div>
</div> <!-- container -->

<?php require 'inc/footer.php' ?>