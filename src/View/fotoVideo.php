<?php require 'inc/header.php' ?>

<style>
<?php if (isset($_GET['view']) && $_GET['view'] == 'list' ): ?>
    <?php $view = 'list'; ?>

    #btnViewAsList {
        content:url("<?= ROOT_URL ?>assets/img/icone-mostrar-lista-ativo.svg");
    }

    #btnViewAsIcon {
        content:url("<?= ROOT_URL ?>assets/img/icone-mostrar-thumbs-inativo.svg");
    }
<?php else: ?>
    <?php $view = 'icon'; ?>

    #btnViewAsList {
        content:url("<?= ROOT_URL ?>assets/img/icone-mostrar-lista-inativo.svg");
    }

    #btnViewAsIcon {
        content:url("<?= ROOT_URL ?>assets/img/icone-mostrar-thumbs-ativo.svg");
    }
<?php endif ?>
</style>

    <div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="pt-5 text-center">
					<img class="p-5" src="<?= ROOT_URL ?>assets/img/icone-foto-video.svg" alt="FOTO E VÍDEO">
					<h2>FAÇA A BUSCA POR <span class="texto-azul">FOTOS E VÍDEOS</span></h2>
				</div>
			</div>
		</div>

		<div class="row pt-3">
            <div class="col-md-6">
                <?php if ($this->arquivo): ?>
                    <div class="col-sm-12 mb-3">
                        <a class="btn btn-secondary me-4" href="<?= ROOT_URL ?>?p=foto&amp;id=<?= $this->arquivo->parent_id ?><?php if (isset($_GET['view'])) { echo "&view=".$_GET['view']; } ?>">Voltar</a>
                        <img class="mb-3" style="width: 50px;" src="<?= ROOT_URL ?>assets/img/icone-folder-pequeno.svg">
                        <?= $this->arquivo->nome ?>
                    </div>
                <?php endif ?>
            </div>
            <div class="col-md-6 text-end">
                <a href="<?= ROOT_URL ?>?p=foto<?php if (isset($_GET['id'])) { echo "&id=".$_GET['id']; } ?>&view=icon">
                    <img id="btnViewAsIcon" class="float-end me-3" style="width: 45px; height: 45px;">
                </a>
                <a href="<?= ROOT_URL ?>?p=foto<?php if (isset($_GET['id'])) { echo "&id=".$_GET['id']; } ?>&view=list">
                    <img id="btnViewAsList" class="float-end me-3" style="width: 42px; height: 42px; margin-top:2px">
                </a>
                <p class="float-end me-3" style="font-size:0.7em; line-height: 3.5;"><b>ORGANIZAR POR:</b></p>
            </div>
		</div>
		
        <hr class="mt-0">

        <?php if ($view == 'icon'): ?>
            <div id="viewAsIcon" class="pt-5">
                <div class="row">
                    <?php foreach ($this->arquivos as $arquivo): ?>
                        <div class="col-sm-6 col-md-4 col-lg-3 mb-5 text-center">
                            <?php if ($arquivo->tipo == 'pasta'): ?>
                                <a href="<?= ROOT_URL ?>?p=foto&id=<?= $arquivo->id ?><?php if (isset($_GET['view'])) { echo "&view=".$_GET['view']; } ?>">
                                    <img class="mb-3" style="width: 100px;" src="<?= ROOT_URL ?>assets/img/icone-folder.svg">
                                </a>
                            <?php else: ?>
                                <a href="<?= $arquivo->link ?>" target="_blank">
                                    <i class="fas fa-file" style="font-size: 6em;color:#11a1e4;margin-bottom: 10px;"></i>
                                </a>
                            <?php endif ?>
                            <p><?= $arquivo->nome ?></p>
                            <p><?= strftime('%d/%m/%Y %H:%M', strtotime($arquivo->updated_at)) ?></p>
                        </div>
                    <?php endforeach ?>
                </div>
            </div>
        <?php else: ?>
            <div id="viewAsList" class="row pt-5">
                <?php foreach ($this->arquivos as $arquivo): ?>
                    <div class="row mb-3" style="border-bottom: 1px solid #b1b1b1;">
                        <div class="col-2">
                            <?php if ($arquivo->tipo == 'pasta'): ?>
                                <a href="<?= ROOT_URL ?>?p=foto&id=<?= $arquivo->id ?><?php if (isset($_GET['view'])) { echo "&view=".$_GET['view']; } ?>">
                                    <img class="mb-3" style="width: 100px;" src="<?= ROOT_URL ?>assets/img/icone-folder.svg">
                                </a>
                            <?php else: ?>
                                <a href="<?= $arquivo->link ?>" target="_blank" style="margin-left: 20px;">
                                    <i class="fas fa-file" style="font-size: 6em;color:#11a1e4;margin-bottom: 10px;"></i>
                                </a>
                            <?php endif ?>
                        </div>
                        <div class="col-8 pt-4">
                            <p><?= $arquivo->nome ?></p>
                        </div>
                        <div class="col-2 pt-4">
                            <p><?= strftime('%d/%m/%Y %H:%M', strtotime($arquivo->updated_at)) ?></p>
                        </div>
                    </div>
                <?php endforeach ?>
            </div>
        <?php endif ?>

		<div class="row pt-5">
			<!-- <div class="col-sm-12">
				<nav aria-label="Page navigation example">
					<ul class="pagination justify-content-center">
						<li class="page-item"><a class="page-link" href="#">< ANTERIOR</a></li>
						<li class="page-item page-link page-position">2 DE 100</li>
						<li class="page-item"><a class="page-link" href="#">PRÓXIMA ></a></li>
					</ul>
				</nav>
			</div> -->
		</div>
	</div> <!-- container -->

<?php require 'inc/footer.php' ?>