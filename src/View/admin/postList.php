<?php require 'header.php' ?>
<?php require 'sidebar.php' ?>
    

        <div class="col-sm-12 col-md-9">
            <div class="mb-3 text-end">
                <a href="<?= ROOT_URL ?>?p=admin&amp;a=postCreate" class="btn btn-primary">Criar</a>
            </div>

            <table class="table table-hover table-bordered">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Tema</th>
                        <th scope="col">Assunto</th>
                        <th scope="col">Area</th>
                        <th scope="col">Arquivo</th>
                        <th scope="col">Tipo</th>
                        <th scope="col">Ação</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($this->posts as $post): ?>
                        <tr>
                            <th><?= $post->id ?></th>
                            <th><?= $post->tema ?></th>
                            <th><?= $post->assunto ?></th>
                            <th><?= $post->area ?></th>
                            <th><?= $post->arquivo ?></th>
                            <th><?= $post->tipo ?></th>
                            <th>
                                <a href="#" class="btn"><i class="fas fa-pen"></i></a>
                                <a href="#" class="btn"><i class="fas fa-trash"></i></a>
                            </th>
                        </tr>
                    <?php endforeach ?>
                
                </tbody>
            </table>
        </div>
    </div>
</div>

<?php require 'footer.php' ?>