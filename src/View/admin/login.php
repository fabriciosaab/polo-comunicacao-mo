<?php require 'header.php' ?>
<?php require 'msg.php' ?>

<div class="container pt-5">
    <div class="row justify-content-center">
        <div class="col-sm-12 col-md-6 col-lg-4">
            <form action="" method="post">
                <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label">Email:</label>
                    <input type="email" class="form-control" id="email" name="email" required="required">
                </div>
                <div class="mb-3">
                    <label for="exampleInputPassword1" class="form-label">Senha:</label>
                    <input type="password" class="form-control" id="password" name="password" required="required">
                </div>
                <button type="submit" class="btn btn-primary">Login</button>
            </form>
        </div>
    </div>
</div>

<?php require 'footer.php' ?>