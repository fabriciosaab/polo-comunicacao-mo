<?php require 'header.php' ?>
<?php require 'sidebar.php' ?>
    

        <div class="col-sm-12 col-md-9">

            <form action="" method="post">  
                <div class="mb-3">
                    <label class="form-label">Tema:</label>
                    <input type="text" class="form-control" name="tema" required="required" value="<?= $post->tema ?>">
                </div>
                <div class="mb-3">
                    <label class="form-label">Assunto:</label>
                    <input type="text" class="form-control" name="assunto" required="required">
                </div>
                <div class="mb-3">
                    <label class="form-label">Area:</label>
                    <input type="text" class="form-control" name="area" required="required">
                </div>
                <div class="mb-3">
                    <label class="form-label">Arquivo:</label>
                    <input type="text" class="form-control" name="arquivo" required="required">
                </div>
                <div class="mb-3">
                    <label class="form-label">Tipo:</label>
                    <input type="text" class="form-control" name="tipo" required="required">
                </div>
                <button type="submit" class="btn btn-primary">Login</button>
            </form>

            <div class="mb-3 text-end">
                <a href="<?= ROOT_URL ?>?p=admin&amp;a=postList" class="btn btn-danger">Cancelar</a>
            </div>

            
        </div>
    </div>
</div>

<?php require 'footer.php' ?>