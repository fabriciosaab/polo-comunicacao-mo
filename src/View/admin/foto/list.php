<?php require __DIR__ . '/../header.php' ?>
<?php require __DIR__ . '/../sidebar.php' ?>
    

        <div class="col-sm-12 col-md-9">

            <form action="<?= ROOT_URL ?>?p=foto&amp;a=criarPasta" method="post">
                <input type="hidden" class="form-control" name="parent_id" value="<?php if(isset($_GET['id'])) { echo $_GET['id']; } else { echo "0"; } ?>">
                <div class="row mb-5">
                    <label class="form-label">Criar Pasta:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="pasta" required="required">
                    </div>
                    <div class="col-sm-2">
                        <button type="submit" class="btn btn-primary w-100">Criar</button>
                    </div>
                </div>
            </form>

            <form action="<?= ROOT_URL ?>?p=foto&amp;a=uploadArquivo" method="post" enctype="multipart/form-data">
                <input type="hidden" class="form-control" name="parent_id" value="<?php if(isset($_GET['id'])) { echo $_GET['id']; } else { echo "0"; } ?>">
                <div class="row mb-3">
                    <label class="form-label">Fazer Upload de Arquivo:</label>
                    <div class="col-sm-10">
                        <input type="file" class="form-control" name="arquivo" required="required">
                    </div>
                    <div class="col-sm-2">
                        <button type="submit" class="btn btn-primary w-100">Upload</button>
                    </div>
                </div>
            </form>
            <div class="mb-5"></div>
            <?php if ($this->arquivo): ?>
                <div class="col-sm-12 text-end mb-3">
                    <a class="btn btn-warning" href="<?= ROOT_URL ?>?p=foto&amp;a=list&amp;id=<?= $this->arquivo->parent_id ?>">Acima</a>
                </div>
            <?php endif ?>

            <table class="table table-hover table-bordered">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nome</th>
                        <th scope="col">Ação</th>
                    </tr>
                </thead>
                <tbody>
                    
                    <?php foreach ($this->arquivos as $arquivo): ?>
                        <tr>
                            <td><?= $arquivo->id ?></td>
                            <td>
                                <?php if ($arquivo->tipo == 'arquivo'): ?>
                                    <a href="<?= $arquivo->link ?>" target="_blank">
                                        <i class="fas fa-file"></i> <?= $arquivo->nome ?></td>
                                    </a>
                                <?php else: ?>
                                    <a href="<?= ROOT_URL ?>?p=foto&amp;a=list&amp;id=<?= $arquivo->id ?>">
                                        <i class="fas fa-folder"></i> <?= $arquivo->nome ?></td>
                                    </a>
                                <?php endif ?>
                            <td class="text-center">
                                <?php if ($arquivo->tipo == 'pasta'): ?>
                                    <a href="<?= ROOT_URL ?>?p=foto&amp;a=edit&amp;id=<?= $arquivo->id ?>" class="btn"><i class="fas fa-pen"></i></a>
                                <?php endif ?>
                                <a href="<?= ROOT_URL ?>?p=foto&amp;a=destroy&amp;id=<?= $arquivo->id ?>" class="btn"><i class="fas fa-trash"></i></a>
                            </td>
                        </tr>
                    <?php endforeach ?>
                
                </tbody>
            </table>
        </div>
    </div>
</div>

<?php require __DIR__ . '/../footer.php' ?>