<?php require __DIR__ . '/../header.php' ?>
<?php require __DIR__ . '/../sidebar.php' ?>
    

        <div class="col-sm-12 col-md-9">
            <div class="mb-3 text-end">
                <a href="<?= ROOT_URL ?>?p=tema&amp;a=temaCreate" class="btn btn-primary">Criar</a>
            </div>

            <table class="table table-hover table-bordered">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Tema</th>
                        <th scope="col">Ação</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($this->temas as $tema): ?>
                        <tr>
                            <td><?= $tema->id ?></td>
                            <td><?= $tema->tema ?></td>
                            <td>
                                <a href="<?= ROOT_URL ?>?p=tema&amp;a=temaEdit&amp;id=<?= $tema->id ?>" class="btn"><i class="fas fa-pen"></i></a>
                                <a href="<?= ROOT_URL ?>?p=tema&amp;a=temaDestroy&amp;id=<?= $tema->id ?>" class="btn"><i class="fas fa-trash"></i></a>
                            </td>
                        </tr>
                    <?php endforeach ?>
                
                </tbody>
            </table>
        </div>
    </div>
</div>

<?php require __DIR__ . '/../footer.php' ?>