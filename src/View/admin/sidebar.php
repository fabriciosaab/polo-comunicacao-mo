<div class="container">
    <div class="row">
        <div class="col-sm-12 col-md-3">
            <div class="d-flex flex-column flex-shrink-0 p-3 bg-light" style="width: 220px;">
                <hr>
                <ul class="nav nav-pills flex-column mb-auto">
                    <li class="nav-item">
                        <a href="<?= ROOT_URL ?>?p=post&amp;a=postList" class="nav-link link-dark <?php if ($_GET['p'] == 'post'): ?> active <?php endif ?>">
                            Arquivos
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="<?= ROOT_URL ?>?p=tema&amp;a=temaList" class="nav-link link-dark <?php if ($_GET['p'] == 'tema'): ?> active <?php endif ?>">
                            Tema
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="<?= ROOT_URL ?>?p=area&amp;a=areaList" class="nav-link link-dark <?php if ($_GET['p'] == 'area'): ?> active <?php endif ?>">
                            Departamento/Área
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="<?= ROOT_URL ?>?p=foto&amp;a=list" class="nav-link link-dark <?php if ($_GET['p'] == 'foto'): ?> active <?php endif ?>">
                            Foto e Vídeo
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="<?= ROOT_URL ?>?p=pagina" class="nav-link link-dark <?php if ($_GET['p'] == 'pagina'): ?> active <?php endif ?>">
                            Páginas
                        </a>
                    </li>
                </ul>
                
                <hr>
            
                <div class="dropdown">
                    <a href="#" class="d-flex align-items-center link-dark text-decoration-none dropdown-toggle" id="dropdownUser2" data-bs-toggle="dropdown" aria-expanded="false">
                        <i class="fas fa-user me-2"></i>
                        <strong>Admin</strong>
                    </a>
                    <ul class="dropdown-menu text-small shadow" aria-labelledby="dropdownUser2">
                        <li><a class="dropdown-item" href="<?= ROOT_URL ?>?p=admin&amp;a=changePassword">Alterar Senha</a></li>
                        <li><hr class="dropdown-divider"></li>
                        <li><a class="dropdown-item" href="<?= ROOT_URL ?>?p=admin&amp;a=logout">Sair</a></li>
                  </ul>
                </div>
            </div>
        </div>
   