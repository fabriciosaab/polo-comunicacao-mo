<?php require __DIR__ . '/../header.php' ?>
<?php require __DIR__ . '/../msg.php' ?>
<?php require __DIR__ . '/../sidebar.php' ?>

        <div class="col-sm-12 col-md-9">
            <form action="<?= ROOT_URL ?>?p=area&amp;a=areaStore" method="post">  
                <div class="mb-3">
                    <label class="form-label">Area:</label>
                    <input type="text" class="form-control" name="area" required="required">
                </div>
                <div class="mb-3 text-end">
                    <a href="<?= ROOT_URL ?>?p=area&amp;a=areaList" class="btn btn-danger">Cancelar</a>
                    <button type="submit" class="btn btn-primary">Criar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<?php require __DIR__ . '/../footer.php' ?>