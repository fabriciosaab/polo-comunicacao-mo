<?php require __DIR__ . '/../header.php' ?>
<?php require __DIR__ . '/../sidebar.php' ?>
    

        <div class="col-sm-12 col-md-9">
            <div class="mb-3 text-end">
                <a href="<?= ROOT_URL ?>?p=area&amp;a=areaCreate" class="btn btn-primary">Criar</a>
            </div>

            <table class="table table-hover table-bordered">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Area</th>
                        <th scope="col">Ação</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($this->areas as $area): ?>
                        <tr>
                            <td><?= $area->id ?></td>
                            <td><?= $area->area ?></td>
                            <td>
                                <a href="<?= ROOT_URL ?>?p=area&amp;a=areaEdit&amp;id=<?= $area->id ?>" class="btn"><i class="fas fa-pen"></i></a>
                                <a href="<?= ROOT_URL ?>?p=area&amp;a=areaDestroy&amp;id=<?= $area->id ?>" class="btn"><i class="fas fa-trash"></i></a>
                            </td>
                        </tr>
                    <?php endforeach ?>
                
                </tbody>
            </table>
        </div>
    </div>
</div>

<?php require __DIR__ . '/../footer.php' ?>