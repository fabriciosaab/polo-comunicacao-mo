<?php require __DIR__ . '/../header.php' ?>
<?php require __DIR__ . '/../msg.php' ?>
<?php require __DIR__ . '/../sidebar.php' ?>

        <div class="col-sm-12 col-md-9">
            <form action="<?= ROOT_URL ?>?p=post&amp;a=postUpdate&amp;id=<?= $this->post->id ?>" method="post">     <div class="mb-3">
                    <label class="form-label">Tipo:</label>
                    <input type="radio" class="btn-check" name="tipo" id="option1" autocomplete="off" value="Informa" <?php if ($this->post->tipo == "Informa"): ?> checked <?php endif ?>>
                    <label class="btn btn-primary" for="option1">Informa</label>

                    <input type="radio" class="btn-check" name="tipo" id="option2" autocomplete="off" value="Mural" <?php if ($this->post->tipo == "Mural"): ?> checked <?php endif ?>>
                    <label class="btn btn-primary" for="option2">Mural</label>
                </div>
                <div class="mb-3">
                    <label class="form-label">Tema:</label>
                    <select class="form-select" name="tema">
                        <?php foreach ($this->temas as $tema): ?>
                            <option value="<?= $tema->tema ?>" <?php if ($this->post->tema == $tema->tema): ?> selected <?php endif ?>><?= $tema->tema ?></option>
                        <?php endforeach ?>
                    </select>
                </div>
                <div class="mb-3">
                    <label class="form-label">Assunto:</label>
                    <input type="text" class="form-control" name="assunto" required="required" value="<?= $this->post->assunto ?>">
                </div>
                <div class="mb-3">
                    <label class="form-label">Departamento/Área:</label>
                    <select class="form-select" name="area">
                        <?php foreach ($this->areas as $area): ?>
                            <option value="<?= $area->area ?>" <?php if ($this->post->area == $area->area): ?> selected <?php endif ?>><?= $area->area ?></option>
                        <?php endforeach ?>
                    </select>
                </div>
                <div class="mb-3">
                    <label class="form-label">Arquivo:</label>
                    <input type="text" class="form-control disabled" disabled required="required" value="<?= $this->post->arquivo_nome ?>">
                </div>
                <div class="mb-3 text-end">
                    <a href="<?= ROOT_URL ?>?p=post&amp;a=postList" class="btn btn-danger">Cancelar</a>
                    <button type="submit" class="btn btn-primary">Salvar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<?php require __DIR__ . '/../footer.php' ?>