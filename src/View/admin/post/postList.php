<?php require __DIR__ . '/../header.php' ?>
<?php require __DIR__ . '/../sidebar.php' ?>
    

        <div class="col-sm-12 col-md-9">
            <div class="row">
                <div class="mb-3 text-end">
                    <a href="<?= ROOT_URL ?>?p=post&amp;a=postCreate" class="btn btn-primary">Criar</a>
                </div>
            </div>

            <div class="row">
                <table class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Tema</th>
                            <th scope="col">Assunto</th>
                            <th scope="col">Area</th>
                            <th scope="col">Arquivo</th>
                            <th scope="col">Tipo</th>
                            <th scope="col">Ação</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($this->posts as $post): ?>
                            <tr>
                                <td><?= $post->id ?></td>
                                <td><?= $post->tema ?></td>
                                <td><?= $post->assunto ?></td>
                                <td><?= $post->area ?></td>
                                <td><?= $post->arquivo_nome ?></td>
                                <td><?= $post->tipo ?></td>
                                <td>
                                    <a href="<?= ROOT_URL ?>?p=post&amp;a=postEdit&amp;id=<?= $post->id ?>" class="btn"><i class="fas fa-pen"></i></a>
                                    <a href="<?= ROOT_URL ?>?p=post&amp;a=postDestroy&amp;id=<?= $post->id ?>" class="btn"><i class="fas fa-trash"></i></a>
                                </td>
                            </tr>
                        <?php endforeach ?>
                    
                    </tbody>
                </table>
            </div>

            <div class="row pt-5">
                <div class="col-sm-12">
                    <nav aria-label="Page navigation example">
                        <ul class="pagination justify-content-center">
                            <?php if ($this->offset == 0 ): ?>
                                <li class="page-item disabled"><a class="page-link" href="#">< ANTERIOR</a></li>
                            <?php else: ?>
                                <li class="page-item"><a class="page-link" href="<?= ROOT_URL ?>?p=post&a=postList&offset=<?=$this->offset - $this->maxPost ?>">< ANTERIOR</a></li>
                            <?php endif ?>

                            <li class="page-item page-link page-position"><?= $this->offset / $this->maxPost + 1 ?> DE <?= ceil($this->count/$this->maxPost) ?></li>

                            <?php if ($this->offset / $this->maxPost + 1 >= ceil($this->count/$this->maxPost) ): ?>
                                <li class="page-item disabled"><a class="page-link" href="#">PRÓXIMA ></a></li>
                            <?php else: ?>
                                <li class="page-item"><a class="page-link" href="<?= ROOT_URL ?>?p=post&a=postList&offset=<?= $this->offset + $this->maxPost ?>">PRÓXIMA ></a></li>
                            <?php endif ?>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>

<?php require __DIR__ . '/../footer.php' ?>