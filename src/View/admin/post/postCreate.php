<?php require __DIR__ . '/../header.php' ?>
<?php require __DIR__ . '/../msg.php' ?>
<?php require __DIR__ . '/../sidebar.php' ?>

            <div class="col-sm-12 col-md-9">
                <form action="<?= ROOT_URL ?>?p=post&amp;a=postStore" method="post" enctype="multipart/form-data">
                    <div class="mb-3">
                        <label class="form-label">Aba:</label>
                        <input type="radio" class="btn-check" name="tipo" id="option1" autocomplete="off" value="Informa">
                        <label class="btn btn-primary" for="option1">Informa</label>

                        <input type="radio" class="btn-check" name="tipo" id="option2" autocomplete="off" value="Mural">
                        <label class="btn btn-primary" for="option2">Mural</label>
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Tema:</label>
                        <select class="form-select" name="tema">
                            <?php foreach ($this->temas as $tema): ?>
                                <option value="<?= $tema->tema ?>"><?= $tema->tema ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Assunto:</label>
                        <input type="text" class="form-control" name="assunto" required="required">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Departamento/Área:</label>
                        <select class="form-select" name="area">
                            <?php foreach ($this->areas as $area): ?>
                                <option value="<?= $area->area ?>"><?= $area->area ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Arquivo:</label>
                        <input type="file" class="form-control" name="arquivo" required="required">
                    </div>
                    <div class="mb-3 text-end">
                        <a href="<?= ROOT_URL ?>?p=post&amp;a=postList" class="btn btn-danger">Cancelar</a>
                        <button type="submit" class="btn btn-primary">Criar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<?php require __DIR__ . '/../footer.php' ?>