<!doctype html>
<html lang="pt-br">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<title><?=\TestProject\Engine\Config::SITE_NAME?></title>

	<!-- Bootstrap core CSS -->
	<link href="<?= ROOT_URL ?>assets/vendor/bootstrap-5.1.3/css/bootstrap.min.css" rel="stylesheet">
	
	<!-- Custom styles for this template -->
	<link href="<?= ROOT_URL ?>assets/css/styles.css" rel="stylesheet">

    <!-- font Awesome -->
    <link href="<?= ROOT_URL ?>assets/vendor/fontawesome-free-5.15.4-web/css/all.css" rel="stylesheet">
</head>

<body>
	<nav class="navbar mb-5">
		<div class="container-fluid">
			<a class="navbar-brand" href="#">
				<img class="logo" src="<?= ROOT_URL ?>assets/img/logo-sabesp.svg" alt="logo Sabesp">
			</a>
		</div>
	</nav>

    <main role="main">
        