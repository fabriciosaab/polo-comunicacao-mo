<?php require 'header.php' ?>
<?php require 'msg.php' ?>
<?php require 'sidebar.php' ?>

		<div class="col-sm-12 col-md-9">
			<p>Alterar Senha</p>
            <form action="<?= ROOT_URL ?>?p=pagina&amp;a=updatePassword" method="post">
                <div class="mb-3">
                    <label class="form-label">Senha atual:</label>
                    <input type="password" class="form-control" name="current_password" required="required">
                </div>

                <div class="mb-3">
                    <label class="form-label">Senha nova:</label>
                    <input type="password" class="form-control" name="new_password">
                </div>

                <div class="mb-3">
                    <label class="form-label">Confirmar senha:</label>
                    <input type="password" class="form-control" name="password_confirmation" required="required">
                </div>

                <div class="mb-3 text-end">
                    <button type="submit" class="btn btn-primary">Atualizar</button>
                </div>
            </form>
        </div>
	</div>
</div>
<?php require 'footer.php' ?>