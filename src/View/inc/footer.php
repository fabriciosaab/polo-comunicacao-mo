    <div class="pt-5"></div>
    <div class="linha-azul"></div>

    <div class="w-100 bg-cinza texto-branco pt-5 pb-5">
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <img class="logo" src="<?= ROOT_URL ?>assets/img/logo-sabesp.svg" alt="logo Sabesp">
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12 col-lg-3">
                    <div class="row">
                        <p class="fs-4">SABESP MO</p>
                        <p>AT VERO EOS ET
                        ACCUSAMUS
                        et iusto odio dignissimos
                        ducimus qui blanditiis</p>
                    </div>
                </div>
                <div class="col-sm-12 col-lg-9 pt-sm-3">
                    <div class="row">
                        <div class="col-sm-12 col-md-3">
                            <div class="row">
                                <p class="fs-4">LINKS ONE</p>
                                <p>AT VERO EOS ET
                                ACCUSAMUS
                                et iusto odio dignissimos
                                ducimus qui blanditiis</p>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-3">
                            <div class="row">
                                <p class="fs-4">LINKS TWO</p>
                                <p>AT VERO EOS ET
                                ACCUSAMUS
                                et iusto odio dignissimos
                                ducimus qui blanditiis</p>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-3">
                            <div class="row">
                                <p class="fs-4">LINKS THREE</p>
                                <p>AT VERO EOS ET
                                ACCUSAMUS
                                et iusto odio dignissimos
                                ducimus qui blanditiis</p>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-3">
                            <div class="row">
                                <p class="fs-4">LINKS FOUR</p>
                                <p>AT VERO EOS ET
                                ACCUSAMUS
                                et iusto odio dignissimos
                                ducimus qui blanditiis</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

    <script src="<?= ROOT_URL ?>assets/vendor/jquery-3.6.0/jquery.min.js"></script>
    <script src="<?= ROOT_URL ?>assets/vendor/bootstrap-5.1.3/js/bootstrap.bundle.min.js"></script>

</body>
</html>