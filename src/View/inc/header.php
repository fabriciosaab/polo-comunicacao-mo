<!doctype html>
<html lang="pt-br">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<title><?=\TestProject\Engine\Config::SITE_NAME?></title>

	<!-- Bootstrap core CSS -->
	<link href="<?=ROOT_URL?>assets/vendor/bootstrap-5.1.3/css/bootstrap.min.css" rel="stylesheet">
	
	<!-- Custom styles for this template -->
	<link href="<?=ROOT_URL?>assets/css/styles.css" rel="stylesheet">

    <!-- font Awesome -->
    <link href="<?= ROOT_URL ?>assets/vendor/fontawesome-free-5.15.4-web/css/all.css" rel="stylesheet">
</head>

<body>
	<nav class="navbar">
		<div class="container-fluid">
			<a class="navbar-brand" href="#">
				<img class="logo" src="<?=ROOT_URL?>assets/img/logo-sabesp.svg" alt="logo Sabesp">
			</a>
		</div>
	</nav>

    <main role="main">
        <div class="container-fluid destaque">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <img src="<?= ROOT_URL ?>assets/img/seta-polo-comunicacao-mo.svg" alt="">
                        <br><br>
                        <p class="texto-branco"><?= $this->destaque ?></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <ul class="nav">
                            <li class="nav-item">
                                <a class="texto-branco nav-link text-center <?php if (CURRENT_PAGE == 'home') { echo "active-mo"; } ?>" href="<?= ROOT_URL ?>">MO INFORMA</a>
                            </li>
                            <li class="nav-item">
                                <a class="texto-branco nav-link text-center <?php if (CURRENT_PAGE == 'mural') { echo "active-mo"; } ?>" href="<?= ROOT_URL ?>?p=mural">MO MURAL</a>
                            </li>
                            <li class="nav-item">
                                <a class="texto-branco nav-link text-center <?php if (CURRENT_PAGE == 'foto') { echo "active-mo"; } ?>" href="<?= ROOT_URL ?>?p=foto">FOTO E VÍDEO</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>