-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Tempo de geração: 22/02/2022 às 00:17
-- Versão do servidor: 10.5.13-MariaDB-cll-lve
-- Versão do PHP: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `u384080986_polo_mo`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `Admins`
--

CREATE TABLE `Admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(120) NOT NULL,
  `password` char(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Despejando dados para a tabela `Admins`
--

INSERT INTO `Admins` (`id`, `email`, `password`) VALUES
(1, 'test@test.com', '$2y$14$kefF6aqkuOEWo7CIFduNf.7O8BuGR4uWrIAFcHWm2u99OcLPDFWOe');

-- --------------------------------------------------------

--
-- Estrutura para tabela `Areas`
--

CREATE TABLE `Areas` (
  `id` int(11) NOT NULL,
  `area` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Despejando dados para a tabela `Areas`
--

INSERT INTO `Areas` (`id`, `area`, `created_at`, `updated_at`) VALUES
(1, 'MO/Qualidade', '2022-01-21 20:27:55', '2022-01-21 20:27:55'),
(2, 'MO/Voluntario', '2022-01-21 20:28:12', '2022-01-21 20:29:59');

-- --------------------------------------------------------

--
-- Estrutura para tabela `FotoVideo`
--

CREATE TABLE `FotoVideo` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT 0,
  `path` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `link` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `tipo` varchar(20) COLLATE utf8mb4_bin NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Despejando dados para a tabela `FotoVideo`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `Paginas`
--

CREATE TABLE `Paginas` (
  `destaque_informa` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `destaque_mural` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `destaque_foto` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Despejando dados para a tabela `Paginas`
--

INSERT INTO `Paginas` (`destaque_informa`, `destaque_mural`, `destaque_foto`) VALUES
('Texto de destaque do informa. Alterar no admin guia Páginas', 'Texto de destaque do mural. Alterar no admin guia Páginas', 'Texto de destaque do foto e video. Alterar no admin guia Páginas');

-- --------------------------------------------------------

--
-- Estrutura para tabela `Posts`
--

CREATE TABLE `Posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `tema` varchar(255) DEFAULT NULL,
  `assunto` varchar(255) DEFAULT NULL,
  `area` varchar(255) DEFAULT NULL,
  `path` varchar(255) DEFAULT NULL,
  `arquivo_nome` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `tipo` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Despejando dados para a tabela `Posts`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `Temas`
--

CREATE TABLE `Temas` (
  `id` int(11) NOT NULL,
  `tema` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Despejando dados para a tabela `Temas`
--

INSERT INTO `Temas` (`id`, `tema`, `created_at`, `updated_at`) VALUES
(1, 'tema 1', '2022-01-21 20:05:00', '2022-01-24 19:37:16'),
(2, 'tema 2', '2022-01-21 20:16:38', '2022-01-24 19:37:21');

--
-- Índices para tabelas despejadas
--

--
-- Índices de tabela `Admins`
--
ALTER TABLE `Admins`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `Areas`
--
ALTER TABLE `Areas`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `FotoVideo`
--
ALTER TABLE `FotoVideo`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `Posts`
--
ALTER TABLE `Posts`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `Temas`
--
ALTER TABLE `Temas`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT para tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `Admins`
--
ALTER TABLE `Admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `Areas`
--
ALTER TABLE `Areas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de tabela `FotoVideo`
--
ALTER TABLE `FotoVideo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de tabela `Posts`
--
ALTER TABLE `Posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT de tabela `Temas`
--
ALTER TABLE `Temas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
