<?php

namespace TestProject\Engine;

final class Config
{
    // Database info (if you want to test the script, please edit the below constants with yours)
    const
    DB_HOST = 'localhost',
    DB_NAME = 'phpmvc',
    DB_USR = 'root',
    DB_PWD = 'root',

    // Title of the site
    SITE_NAME = 'Polo Comunicação MO',

    // Storage path
    STORAGE_PATH = '/home/fabricio/Dev/PHP-MVC/storage/',

    // Storage Link
    STORAGE_LINK = ROOT_URL . "storage/";
}