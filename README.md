## Requisitos
- Servidor web(Apache, Nginx, IIS)
- PHP 5.5 ou maior
- Banco Mysql

## Instalação
- Descomprimir todos os arquivos na raiz do servidor
- Configurar as credênciais do banco no arquivo Engine/Config.php
- Importar o arquivo db.sql no Banco de Dados
- Dar permissão na pasta storage